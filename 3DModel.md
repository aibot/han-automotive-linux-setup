# Creating and importing 3D Model



### Basic Guidlines:
These are just basic guidelines for the creation of the model, you don't have to follow them at all, but I think its better to do it.

#### 1. Create the model in Solidworks
Keep it a bit simple, every special shape is going to make your model heavier and we don't want that. 
Notice that you want to seperate the moving parts from the solid parts, in our case that mean that the tractor body and bin are made as one part, and the wheels as another.
Also note that in the case of the H2Trac, the wheel model that was provided had very detailed tires, and so they had to be simplified.

** Do not forget to mark down the exact measurements and where all of the parts are connected (for example wheel to body)**

#### 2. Export to .stl
In order to use this model with gazebo, it has to be converted to a .dea file. We will do this on Linux later. 
To have the option to convert it, we must export the model from solidworks to a **.stl** format. Do that.

#### 3. Converting to .dea
Download freeCAD to your linux, or use any other program that can convert the .stl file to a .dea file.
When using freeCAD I made sure that the origin of each model is placed in the right place, I suggest you do the same.
This also has to do with the measurements you took from step 1. 
When working with the Gazebo and .SDF files, the location of each part will be from its origin, so the measurements should be taken from the origin of each model.

Export each part of the model to a .dea file.


------> Notice: in many tutorials its clamed that the size of the model should be converted in "Blender" from mm to m, I didn't have to do that, I don't know why, but my model was already in meters.

#### 4. Actually building the model for Gazebo

Quickly about SDF files:
It's just a simple text/xml file representing a model. each part of the model is named a link. Each link has a couple of properties, the ones that interest us are **collision** and **visual**. Each SDF file should also come with a config file that is basically its ID.
Each part has its own coordinated: <0 0 0 0 0 0> == <x y z roll pitch yaw>

There are 2 ways to do that:
+ Text editor
+ Inside Gazebo

The text editor version will give a simpler looking text file, which is nice, but you'll have to visualize the model many times to make sure everything is placed right.
Doing the same thing through Gazebo will yeild an .SDF file which has more parameters for each part of the model, but the visualization is instant.
Thus I advice to use Gazebo. 
Just open gazebo, and inside you can  do **Edit->Model Editior or Ctrl+M** .

Import the base part of your model, place it in <0 0 0 0 0 0> , add all the other parts to your model, notice that the measurements are in **meters** and the roll/pitch/yaw in **radians**.

**Note:** For some idiotic reason, when you edit the pose in gazebo, the first option is for the inertial of the object and not the pose itself. You MUST scroll down to the real pose setting.

**Note 2:** for some reason gazebo ignored the names of the links when I build the model, so you should save the model when all the links are placed, go to the .SDF file itself, and change all the names of the links from "link_0" to "base"/"wheel"/"whatever"


Now we will create joints between the links. Open the model again in gazebo - HUH, no editing option, of course.
To edit the model, you have to insert it into the world, then **Right click -> Edit Model**.
Add the joints, I used "revolute" for the wheels, I hope this is correct. In the case of the H2Trac tractor the joints are only between the wheels and the body.

Now that its done, save the model.

Well done, model is done. You can check that everything seems correct by inserting the model into the world and seeing that it is displayed correctly and acts correctly (play the simulation, the button is at the bottom of the screen.
When I first had a problem with the joints, when the simulation was running the wheels were falling off in a weird way, or there was some collision between the parts which launch the model to the sky breaking it completely.


**Note 3:** The paths in the SDF file are **NOT** relative, that means that if you move the .dea meshes or move the SDF file to a different PC you will have to update the path of the dea files IN THE SDF file. I haven't found a way to escape that, maybe I can create a script to change the path, but this won't be done at the moment.

#### 5. Inserting into a .world file
The Gazebo simulations use a world file, which describes the world the models are in, we have to include the model that we've built into the world that we are going to use.

The model should be inserted as follows:
```xml
 		<include>
			<uri>model://H2Track_model_2</uri>
           		 <pose>3 2 0 0 0 0 </pose>
		</include> 
```
(Pose should be where ever you want)

Note that it addresses "model://"
This will look up the model in the folders that are defined under the enviromental variable: $GAZEBO_MODEL_PATH
Thus you should either move the model to a folder in that path (you can check all the paths with "echo $GAZEBO_MODEL_PATH" or you can add the model path to this variable using:
```export GAZEBO_MODEL_PATH=${GAZEBO_MODEL_PATH}:path-to-your-model```

now to check that everything is working correctly you can launch the world file, and you should see the model in that world. 
If it is dark, make sure that you have a "sun" model in your world:
```xml 
		<!-- A global light source -->
		<include>
			<uri>model://sun</uri>
		</include>
```

