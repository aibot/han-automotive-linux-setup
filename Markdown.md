**Table of contents**

[[_TOC_]]

# General about Markdown


This wiki is written with [Markdown](https://en.wikipedia.org/wiki/Markdown).
Markdown is a lightweight markup language with plain-text-formatting syntax.

## Linklist

[Quickstart Guide](https://www.markdownguide.org/getting-started/)

[Basic Syntax](https://www.markdownguide.org/basic-syntax/)

# Setup of the wiki Environment

For Git see [here](./Git.md)

## Editor
[Atom](https://atom.io/) is a good editor for different languages, such as Markup. Atom [can be extended](https://www.portent.com/blog/copywriting/content-strategy/atom-markdown.htm) with packages to make editing easier:
1. language-markdown
2. markdown-writer
3. markdown-preview


# collection of useful commands

## Keyboard keys
To write keyboard keys, just put \<kbd> \[The desired keyboard key] \</kbd>

## Links
To create a link, write the text in square brackts, and put the link address in curved brackets with no space after it
\[Text](https://Linkaddress)

### link documents to each other
Links can also be used to  link to other documents in the wiki. Instead of a linkadress provide a relative path to the desired file: *./home.md*

## Arrows
You can create Arrows with \&larr;, \&uarr;, \&rarr; and \&darr; giving &larr;, &uarr;, &rarr; and &darr;


# 


